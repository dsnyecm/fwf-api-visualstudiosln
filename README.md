# fwf-api-visualStudioSln

## Description
This is the main Visual Studio solution for the Fleet Web Forms back end.

## Developer Setup
This repository contains git submodules.  If you are not familiar with submodules please review: [https://git-scm.com/book/en/v2/Git-Tools-Submodules]()

Hint: git clone --recurse-submodules -j8 https://bitbucket.org/dsnyecm/fwf-api-visualstudiosln.git

For work on your own feature branch, it is acceptable to convert package references to project references for convenience.  However, any commits to a deployment branch (dev, stg, master) must contain ONLY NuGet package references to EXISTING .nupkg artifiacts!

* pmforms -> Docker
* commonInfrastructure -> NuGet package in Artifactory
* dataObjects -> NuGet package in Artifactory
* gateway -> Docker
* jobScheduler -> Docker
* notification -> Docker

### Running Locally
On the first run, you will likely encounter this exception: `RabbitMQ.Client.Exceptions.BrokerUnreachableException`
Even though the docker-compose has a `depends_on` section, the RabbitMQ is never fully initialized before the services try to connect.  Just run the docker-compose a second time and the services should connect correctly as the RabbitMQ service has now had time to fully initialize.

## Configuration
Settings are passed by environment variables in the following order of precedence:

1. Clould Config
2. Environment
3. appsettings.json

Settings in `appsettings.json` files should kept to a minimum and should only be considered as fallback or default value if a value is not found in any of the proper "non-static" places.

Note: to override a value that is nested in .json, use a double-underscore as the separator

### Local Development
Developers local secrets should be kept in the `secrets.env` file and not committed into source control.  The following should be included in the secrets.env for local developement:

* `SPRING_CLOUD_CONFIG_SERVER_GIT_URI`
* `SPRING_CLOUD_CONFIG_SERVER_GIT_USERNAME`
* `SPRING_CLOUD_CONFIG_SERVER_GIT_PASSWORD`
* `HTTP_PROXY`
* `HTTPS_PROXY`
* `DB_CONNECTION_STRING`
* `HANGFIRE_CONNECTION_STRING`

### Development Deployment
Secrets are set within Rancher environment.
### Staging Deployment
Secrets are set within Rancher environment.
### Production Deployment
Secrets are set within Rancher environment as determined by DevOps.

### Value Keys
* `ASPNETCORE_ENVIRONMENT` set to Development, LocalDevelopment, Staging, or Production to set which appsettings.json file will be read.
* `DB_CONNECTION_STRING`
* `HANGFIRE_CONNECTION_STRING` on deployments environments this should be the same as the main database, for local development be sure to USE YOUR OWN PERSONAL DATABASE!
* `HTTP_PROXY` as "http://username.password@proxy.server:port"
* `HTTPS_PROXY` as "http://username.password@proxy.server:port"
* `NO_PROXY` hostnames of other services internal to Rancher should be included in comma separated list.
* `SPRING_CLOUD_CONFIG_SERVER_GIT_URI`
* `SPRING_CLOUD_CONFIG_SERVER_GIT_USERNAME`
* `SPRING_CLOUD_CONFIG_SERVER_GIT_PASSWORD`

