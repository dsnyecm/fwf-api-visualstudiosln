#!/bin/bash

artPath="msplva-dsnsmt71.csc.nycnet:5000/dsny-fwf"

gatewayTag="tmp"
formsTag="tmp"
jobSchedulerTag="tmp"
notificationTag="tmp"

docker build ./fwf-api-gateway/fwf-api-gateway --file ./fwf-api-gateway/fwf-api-gateway/Dockerfile -t $artPath/fwf-api-gateway:$gatewayTag
#docker push $artPath/fwf-api-gateway:$gatewayTag

docker build ./fwf-api-pmforms/fwf-api-pmforms --file ./fwf-api-pmforms/fwf-api-pmforms/Dockerfile -t $artPath/fwf-api-pmforms:$formsTag
#docker push $artPath/fwf-api-pmforms:$formsTag

docker build ./fwf-api-jobscheduler/fwf-api-jobscheduler  --file ./fwf-api-jobscheduler/fwf-api-jobscheduler/Dockerfile -t $artPath/fwf-api-jobscheduler:$jobSchedulerTag
#docker push $artPath/fwf-api-jobscheduler:$jobSchedulerTag

docker build ./fwf-api-notification/fwf-api-notification --file ./fwf-api-notification/fwf-api-notification/Dockerfile -t $artPath/fwf-api-notification:$notificationTag
#docker push $artPath/fwf-api-notification:$notificationTag

